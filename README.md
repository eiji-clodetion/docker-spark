replicas en docker-compose.yml solo funciona con docker swarm. Para escalar utilizar:
  docker-compose scale worker=3

Para ejecutar docker-compose en modo detach:
  docker-compose up -d

Para ver logs de los containers:
  docker-compose logs
  docker-compose master
  docker-compose worker

Para ver la URL de jupyter con token:
  docker-compose logs | grep "?token="
